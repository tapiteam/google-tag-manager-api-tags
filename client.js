'use strict'

const google = require('googleapis')
const readline = require('readline')
const gtm = google.tagmanager({ version: 'v1' })
const extend = require('util')._extend

const SCOPES = [
  'https://www.googleapis.com/auth/tagmanager.readonly',
  'https://www.googleapis.com/auth/tagmanager.edit.containers',
  'https://www.googleapis.com/auth/tagmanager.delete.containers',
  'https://www.googleapis.com/auth/tagmanager.edit.containerversions',
  'https://www.googleapis.com/auth/tagmanager.publish',
  'https://www.googleapis.com/auth/tagmanager.manage.users',
  'https://www.googleapis.com/auth/tagmanager.manage.accounts'
]

class GTMClient {
  constructor (args) {
    this.clientId = args.clientId
    this.clientSecret = args.clientSecret
    this.redirectUrl = args.redirectUrl
    this.authorizationCode = args.authorizationCode || null
    this.refreshToken = args.refreshToken

    if (this.refreshToken) {
      this.auth = this.getAuth(this.refreshToken)
    }
  }

  getParams (target, add) {
    let params = {
      auth: this.auth,
      accountId: target.accountId,
      containerId: target.containerId
    }

    return add ? extend(params, add) : params
  }

  getAuth (refreshToken) {
    let auth = new google.auth.OAuth2(this.clientId, this.clientSecret,
      this.redirectUrl)

    auth.setCredentials({ refresh_token: refreshToken })
    return auth
  }

  getTokens () {
    let auth = new google.auth.OAuth2(this.clientId, this.clientSecret,
      this.redirectUrl)

    let authUrl = this.auth.generateAuthUrl({
      access_type: 'offline',
      scope: SCOPES
    })
    console.log('Authorize app by visiting this URL:\n', authUrl)

    let rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout
    })

    rl.question('Enter authorization code: ', (code) => {
      rl.close()
      auth.getToken(code, (err, tokens) => {
        if (err) {
          console.log('Error retrieving access tokens.\n', err)
          return
        }

        console.log(tokens)
      })
    })
  }

  //
  // Tag management.
  //

  // https://developers.google.com/tag-manager/api/v1/reference/accounts/containers/tags/list
  listTags (target, callback) {
    let params = this.getParams(target)
    gtm.accounts.containers.tags.list(params, (err, data) => {
      if (err) {
        return console.log(err)
      }

      callback(data)
    })
  }

  // https://developers.google.com/tag-manager/api/v1/reference/accounts/containers/tags/get
  getTagById (target, id, callback) {
    let params = this.getParams(target, { tagId: id })
    gtm.accounts.containers.tags.get(params, (err, data) => {
      if (err) {
        return console.log(err)
      }

      callback(data)
    })
  }

  // https://developers.google.com/tag-manager/api/v1/reference/accounts/containers/tags/create
  createTag (target, name, value, callback) {
    let params = this.getParams(target, {
      resource: {
        name: name,
        type: 'html',
        liveOnly: false,
        parameter: [{
          type: 'template',
          key: 'html',
          value: value
        }, {
          type: 'boolean',
          key: 'supportDocumentWrite',
          value: 'false'
        }]
      }
    })

    gtm.accounts.containers.tags.create(params, (err, data) => {
      if (err) {
        return console.log(err)
      }

      callback(data)
    })
  }

  // https://developers.google.com/tag-manager/api/v1/reference/accounts/containers/tags/update
  updateTag (target, tag, callback) {
    let params = this.getParams(target, {
      tagId: tag.tagId,
      resource: tag
    })

    gtm.accounts.containers.tags.update(params, (err, data) => {
      if (err) {
        return console.log(err)
      }

      callback(data)
    })
  }

  // https://developers.google.com/tag-manager/api/v1/reference/accounts/containers/tags/delete
  deleteTagById (target, id, callback) {
    let params = this.getParams(target, { tagId: id })
    gtm.accounts.containers.tags.delete(params, (err, data) => {
      if (err) {
        return console.log(err)
      }

      callback(data)
    })
  }

  getTagByName (target, name, callback) {
    this.hasTag(target, name, (tag) => {
      if (tag === null) {
        return console.log(`Tag does not exist: ${name}`)
      }

      this.getTagById(target, tag.tagId, callback)
    })
  }

  deleteTagByName (target, name, callback) {
    this.hasTag(target, name, (tag) => {
      if (tag === null) {
        return console.log(`Tag does not exist: ${name}`)
      }

      this.deleteTagById(target, tag.tagId, callback)
    })
  }

  hasTag (target, name, callback) {
    this.listTags(target, (data) => {
      let tags = data.tags.filter(tag => tag.name === name)
      return tags.length === 1 ? callback(tags[0]) : callback(null)
    })
  }

  createOrUpdateTag (target, name, value, callback) {
    this.hasTag(target, name, (tag) => {
      if (tag === null) {
        return this.createTag(target, name, value, callback)
      }

      tag.name = name
      tag.parameter[0].value = value
      this.updateTag(target, tag, callback)
    })
  }
}

module.exports.GTMClient = GTMClient
